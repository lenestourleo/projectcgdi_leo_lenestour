#include "geometrycentral/surface/manifold_surface_mesh.h"
#include "geometrycentral/surface/meshio.h"
#include "geometrycentral/surface/vertex_position_geometry.h"
#include "geometrycentral/surface/geometry.h"
#include <geometrycentral/surface/remeshing.h>
#include "geometrycentral/surface/surface_mesh_factories.h"
#include "geometrycentral/numerical/linear_solvers.h"

#include "polyscope/polyscope.h"
#include "polyscope/surface_mesh.h"
#include "polyscope/curve_network.h"
//#include "polyscope/texture.h"

#include <Eigen/Sparse>

using namespace geometrycentral;
using namespace geometrycentral::surface;

// Geometry-central data
std::unique_ptr<ManifoldSurfaceMesh> meshSource;
std::unique_ptr<VertexPositionGeometry> geometrySource;
std::vector<Vector3> curvePoints;
std::vector<std::array<int, 2>> curveEdges;

Eigen::VectorXd boundaryVectorX;
Eigen::VectorXd boundaryVectorY;
Eigen::VectorXd boundaryVectorZ;

double meshLength;

// Function that creates a scalar field from a function
template <typename Function>
std::vector<double> functionToScalarField(Function func)
{
    std::vector<double> scalarValues;
    for (Vertex v : meshSource->vertices()) {
        auto p = geometrySource->vertexPositions[v];
        double scalarValue = func(p);
        scalarValues.push_back(scalarValue);
    }
    return scalarValues;
}

// Function that creates a sparse matrix corresponding to the following system of equation :
// for all vertices i on the boundary of the mesh, x_i=h_i with h_i an array of predefined boundary conditions
// for all other vertices i, \sum_{j \in N(i)} w_{ij} (x_i-x_j) = 0 where w_{ij} is the cotangent weight of the edge (i,j)
Eigen::SparseMatrix<double> createSparseDirichletSystemMatrix()
{
  int n = meshSource->nVertices();
  // Create a sparse matrix
  Eigen::SparseMatrix<double> A(n, n);

  // Create a triplet list to store the non-zero entries
  std::vector<Eigen::Triplet<double>> tripletList;

  // preparing the geometry data does not work if we do not refresh after each step
  geometrySource->requireEdgeCotanWeights();
  // we can use edgeCotanWeight(e) immediate function instead

  VertexData<size_t> vertexIndex(*meshSource);

  int k = 0;
  for(Vertex v : meshSource->vertices()) {
      vertexIndex[v] = k;
      k++;
  }

  int i;
  int j;
  double cotanWeight;
  // Iterate over all neighboring vertices
  for (Edge e : meshSource->edges()) {
    //cotanWeight = geometrySource->edgeCotanWeight(e);
    cotanWeight = geometrySource->edgeCotanWeights[e];
    i=vertexIndex[e.adjacentVertices()[0]];
    j=vertexIndex[e.adjacentVertices()[1]];
    // Add the entry to the triplet list
    if (!e.adjacentVertices()[0].isBoundary()){
      tripletList.push_back(Eigen::Triplet<double>(i, j, cotanWeight));
    }
    if (!e.adjacentVertices()[1].isBoundary()){
      tripletList.push_back(Eigen::Triplet<double>(j, i, cotanWeight));
    }
  }

  double sum;
  for (Vertex v : meshSource->vertices()) {
    i=vertexIndex[v];
    if (v.isBoundary()){
      tripletList.push_back(Eigen::Triplet<double>(i, i, 1));
    } else {
      sum=0;
      for(Edge e : v.adjacentEdges()) {
        sum+=geometrySource->edgeCotanWeights[e];
      }
      // Add the entry to the triplet list
      tripletList.push_back(Eigen::Triplet<double>(i, i, -sum));
    }
  }

  // Set the non-zero entries of the sparse matrix
  A.setFromTriplets(tripletList.begin(), tripletList.end());

  return A;
}

// Function creating a cylinder with only vertical faces
std::tuple<std::unique_ptr<ManifoldSurfaceMesh>, std::unique_ptr<VertexPositionGeometry>> createCylinder(double radius, double height, int n) {
  std::vector<Vector3> vertices;
  std::vector<std::vector<size_t>> faces;

  double angle = 2*M_PI/n;
  double x;
  double z;
  
  // Vertices of first face
  for (int i = 0; i < n; i++) {
    x = radius*cos(i*angle);
    z = radius*sin(i*angle);
    Vector3 v = {x, 0, z};
    vertices.push_back(v);
  }

  // Vertices of second face
  for (int i = 0; i < n; i++) {
    x = radius*cos(i*angle);
    z = radius*sin(i*angle);
    Vector3 v = {x, height, z};
    vertices.push_back(v);
  }

  // Vertical faces
  for (int i = 0; i < n; i++) {
    size_t prev = (i - 1 + n) % n;
    std::vector<size_t> face = {static_cast<size_t>(i), static_cast<size_t>(prev), static_cast<size_t>(prev + n)};
    faces.push_back(face);
    face = {static_cast<size_t>(i), static_cast<size_t>(prev + n), static_cast<size_t>(i + n)};
    faces.push_back(face);
  }

  meshSource.reset(new ManifoldSurfaceMesh(faces));
  geometrySource.reset(new VertexPositionGeometry(*meshSource));

  for (Vertex v : meshSource->vertices()) {
    // Use the low-level indexers here since we're constructing
    (*geometrySource).vertexPositions[v] = vertices[v.getIndex()];
  }

  return std::make_tuple(std::move(meshSource), std::move(geometrySource));
}

// Function taking a mesh and a geometry and creating a curve network of the boundary of the mesh
std::tuple<std::vector<Vector3>, std::vector<std::array<int, 2>>> curveOnBoundary(){
  std::vector<Vector3> curvePoints;
  std::vector<std::array<int, 2>> curveEdges;

  VertexData<size_t> vertexIndex(*meshSource);

  // Add the vertices of the boundary to the curvePoints vector
  int k = 0;
  for (Vertex v : meshSource->vertices()) {
    if (v.isBoundary()){
      curvePoints.push_back(geometrySource->vertexPositions[v]);
      vertexIndex[v] = k;
      k++;
    }
  }
  
  // Add the edges of the boundary to the curveEdges vector
  for (Vertex u : meshSource->vertices()) {
    for (Vertex v : u.adjacentVertices()) {
      if (u.isBoundary() && v.isBoundary()){
        std::array<int, 2> edge = {static_cast<int>(vertexIndex[u]), static_cast<int>(vertexIndex[v])};
        curveEdges.push_back(edge);
      }
    }
  }

  return std::make_tuple(curvePoints, curveEdges);
}

// Function creating the boundary vector for some coordinates (only 0 except on Boundary where the position is kept)
Eigen::VectorXd createBoundaryVector(int coord){
  Eigen::VectorXd boundaryVector(meshSource->nVertices());

  int i=0;
  for (auto v : meshSource->vertices()){
    if (v.isBoundary()){
      switch (coord) {
        case 0:
          boundaryVector[i]=geometrySource->vertexPositions[i].x;
          break;
        case 1:
          boundaryVector[i]=geometrySource->vertexPositions[i].y;
          break;
        case 2:
          boundaryVector[i]=geometrySource->vertexPositions[i].z;
          break;
      }
    } else {
      boundaryVector[i]=0;
    }
    i++;
  }

  return boundaryVector;
}

// Function creating the boundary vector for a curve projected (only 0 except on Boundary where the height is retrieved)
Eigen::VectorXd createBoundaryVectorCurve(){
  Eigen::VectorXd boundaryVector(meshSource->nVertices());

  int i=0;
  for (auto v : meshSource->vertices()){
    if (v.isBoundary()){
      boundaryVector[i]=curvePoints[i].y;
    } else {
      boundaryVector[i]=0;
    }
    i++;
  }

  return boundaryVector;
}

// Function modifying the cube with holes mesh to test different relative sizes for the holes
void cubeDeform(double offset){
  int i=0;
  int x;
  int y;
  int z;
  for (auto v : meshSource->vertices()){
    x=geometrySource->vertexPositions[i].x;
    y=geometrySource->vertexPositions[i].y;
    z=geometrySource->vertexPositions[i].z;
    // Offset vertices depending on their positions
    if (x>0.5){
      geometrySource->vertexPositions[i].x=x+offset;
    }
    if (y>0.5){
      geometrySource->vertexPositions[i].y=y+offset;
    }
    if (z>0.5){
      geometrySource->vertexPositions[i].z=z+offset;
    }
    if (x<-0.5){
      geometrySource->vertexPositions[i].x=x-offset;
    }
    if (y<-0.5){
      geometrySource->vertexPositions[i].y=y-offset;
    }
    if (z<-0.5){
      geometrySource->vertexPositions[i].z=z-offset;
    }
    i++;
  }
}

// Function that takes a function parametrizing a curve and returns a tuple containing the points and edges of the curve
std::tuple<std::vector<Vector3>, std::vector<std::array<int, 2>>> curveEquationToCurve(std::function<Vector3(float)> curveEquation, int numPoints) {
  std::vector<Vector3> curvePoints;
  std::vector<std::array<int, 2>> curveEdges;

  for (int i = 0; i < numPoints; i++) {
      float angle = 2 * M_PI * i / numPoints;
      Vector3 point=curveEquation(angle);
      curvePoints.push_back(point);
      int next=(i+1) % numPoints;
      std::array<int, 2> edge = {i,next};
      curveEdges.push_back(edge);
  }
  return std::make_tuple(curvePoints, curveEdges);
}

// Function that takes an array of the points of a curve, project them on the xz plane, create a surface enclosed by these points and remeshes the surface
// it returns a tuple containing the mesh and the geometry of the remeshed surface
std::tuple<std::unique_ptr<ManifoldSurfaceMesh>, std::unique_ptr<VertexPositionGeometry>> createProjectedSurface(std::vector<Vector3> curvePoints) {
  std::vector<Vector3> vertices;
  // Project the curve points on the xz plane
  for (auto p : curvePoints) {
      Vector3 v = {p.x, 0, p.z};
      vertices.push_back(v);
  }
  // Create the faces of the surface
  std::vector<std::vector<size_t>> faces;
  for (size_t i = 2; i < curvePoints.size(); i++) {
    size_t prev = i-1;
    std::vector<size_t> face = {0, i, prev};
    faces.push_back(face);
  }

  meshSource.reset(new ManifoldSurfaceMesh(faces));
  geometrySource.reset(new VertexPositionGeometry(*meshSource));

  for (Vertex v : meshSource->vertices()) {
    // Use the low-level indexers here since we're constructing
    (*geometrySource).vertexPositions[v] = vertices[v.getIndex()];
  }

  return std::make_tuple(std::move(meshSource), std::move(geometrySource));
}

// Function that merge vertices that are close enough to each other and connect the faces properly
void cleanMesh(double threshold) {
  // Create a vector to store the mapping from original vertices to merged vertices
  std::vector<int> vertexMap(meshSource->nVertices(), -1);
  std::vector<Vector3> vertices;
  std::vector<std::vector<size_t>> faces;

  VertexData<size_t> vertexIndex(*meshSource);
  VertexData<bool> visited(*meshSource, false);
  int k = 0;
  for(Vertex v : meshSource->vertices()) {
    vertexIndex[v] = k++;
  }

  // For each vertex in the original mesh, compare its position to the positions of all other vertices and cluster them together if they are close enough
  for (Vertex v : meshSource->vertices()) {
    if (visited[v]) {
      continue;
    }

    std::vector<Vertex> clusterVertices;
    clusterVertices.push_back(v);
    visited[v] = true;

    // Compare the current vertex to all other vertices
    for (Vertex w : meshSource->vertices()) {
      if (visited[w]) {
        continue; // Skip vertices that have already been visited
      }

      // If the distance between the current vertex and any other vertex is less than the threshold, add it to the current cluster
      if ((geometrySource->vertexPositions[v] - geometrySource->vertexPositions[w]).norm() < threshold) {
        clusterVertices.push_back(w);
        visited[w] = true;
      }
    }

    // Merge the vertices in the cluster and add the merged vertex to the new vertices vector
    Vector3 mergedVertex = {0, 0, 0};
    for (Vertex clusterVertex : clusterVertices) {
      mergedVertex += geometrySource->vertexPositions[clusterVertex];
      vertexMap[vertexIndex[clusterVertex]] = vertices.size();
    }
    vertices.push_back(mergedVertex / clusterVertices.size());
  }

  // For each face in the original mesh, create new faces with merged vertices and no duplicate edges
  for (Face f : meshSource->faces()) {
    std::vector<size_t> face;
    for (Vertex v : f.adjacentVertices()) {
      int idx = vertexMap[vertexIndex[v]];
      if (idx != -1) {
        face.push_back(idx);
      }
    }

    // Test if face is degenerate (has less than 3 unique vertices)
    //std::sort(face.begin(), face.end());
    //face.erase(std::unique(face.begin(), face.end()), face.end());
    //if (face.size() < 3) {
    //  continue;
    //}

    // Or without sorting
    if (face[0]==face[1] || face[0]==face[2] || face[1]==face[2]){
      continue;
    }

    faces.push_back(face);
  }

  std::unique_ptr<ManifoldSurfaceMesh> newMesh(new ManifoldSurfaceMesh(faces));
  std::unique_ptr<VertexPositionGeometry> newGeometry(new VertexPositionGeometry(*newMesh));

  for (Vertex v : newMesh->vertices()) {
    (*newGeometry).vertexPositions[v] = vertices[v.getIndex()];
  }
}

// This function does one iteration of the Dirichlet problem, i.e. it creates and solves the corresponding linear system
// for the current surface and updates the geometry
void iterationDirichlet(){
  // Create the system matrix and the solver
  auto cotanMat=createSparseDirichletSystemMatrix();
  geometrycentral::Solver<double> solver(cotanMat);

  boundaryVectorX=createBoundaryVector(0);
  boundaryVectorY=createBoundaryVector(1);
  boundaryVectorZ=createBoundaryVector(2);

  // Solve the system for each coordinates, possible thanks to linearity
  Vector<double> solX=solver.solve(boundaryVectorX);
  Vector<double> solY=solver.solve(boundaryVectorY);
  Vector<double> solZ=solver.solve(boundaryVectorZ);

  // Update the vertices with the solved coordinates
  int i=0;
  for (auto v : meshSource->vertices()){
    geometrySource->vertexPositions[i].x=solX[i];
    geometrySource->vertexPositions[i].y=solY[i];
    geometrySource->vertexPositions[i].z=solZ[i];
    i++;
  }

  // Ensure that any existing quantities are updated for the new edges
  geometrySource->refreshQuantities();

  polyscope::getSurfaceMesh("Minimal Surface")->updateVertexPositions(geometrySource->vertexPositions);
}

int counter=0;

bool go=false;

// Callback function for the GUI
void myCallback()
{
  if (ImGui::Button("Optimize"))
    go=true;
  
  if (go)
  {
    iterationDirichlet();
    go=false;
  }
  counter++;
}

// THIS IS THE EXAMPLE FILE
// you can generate every figure and tests from this file, here are the uses
// arguments = 1 <mesh_lenght> <num_points> : creates the minimal surface starting from the curve parametrization of the boundary
// arguments = 2 <mesh_lenght> <radius> <height> <num_points> : creates the minimal surface starting from the created cylinder mesh
// arguments = 3 <mesh_lenght> <offset> : creates the minimal surface starting from the imported mesh of the cube with holes and specify the offsets for variations
// arguments = 4 <mesh_lenght> <path_to_mesh> : creates the minimal surface starting from an imported mesh
int main(int argc, char **argv) {

  // Initialize polyscope
  polyscope::init();

  // Check if the user has provided arguments
  if (argc < 2) {
    std::cout << "Usage 1 <mesh_lenght> <num_points>" << std::endl;
    std::cout << "Usage 2 <mesh_lenght> <radius> <height> <num_points>" << std::endl;
    std::cout << "Usage 3 <mesh_lenght> <offset>" << std::endl;
    std::cout << "Usage 4 <mesh_lenght> <path_to_mesh>" << std::endl;
    return EXIT_FAILURE;
  }

  int ex_number = atoi(argv[1]);

  // Depending on the first argument, we create the corresponding minimal surface example
  switch (ex_number) {
    case 1: { // Minimal surface from curve
      if (argc != 4) {
        std::cout << "Usage 1 <mesh_lenght> <num_points>" << std::endl;
        return EXIT_FAILURE;
      }
      auto curveEquation = [](float angle) { Vector3 v={2*cos(angle), 1+cos(5*angle)/5, sin(angle)}; return v; };

      // Create the curve
      int numPoints = atoi(argv[3]);
      std::cout << "numPoints = " << numPoints << std::endl;
      std::tie(curvePoints, curveEdges) = curveEquationToCurve(curveEquation, numPoints);

      auto psCurve = polyscope::registerCurveNetwork("Curve", curvePoints, curveEdges);

      std::tie(meshSource, geometrySource) = createProjectedSurface(curvePoints);

      // Create the system to solve
      auto cotanMat=createSparseDirichletSystemMatrix();

      geometrycentral::Solver<double> solver(cotanMat);

      Eigen::VectorXd boundaryVector=createBoundaryVectorCurve();

      Vector<double> solHeight = solver.solve(boundaryVector);

      for(size_t i = 0; i < meshSource->nVertices(); ++i)
        geometrySource->vertexPositions[i].y=solHeight[i];
      break; }
    case 2: { // Minimal surface from cylinder
      if (argc != 6) {
        std::cout << "Usage 2 <mesh_lenght> <radius> <height> <num_points>" << std::endl;
        return EXIT_FAILURE;
      }
      // Create a cylinder
      float radius = atof(argv[3]);
      float height = atof(argv[4]);
      int numPoints = atoi(argv[5]);
      std::cout << "radius = " << radius << std::endl;
      std::cout << "height = " << height << std::endl;
      std::cout << "numPoints = " << numPoints << std::endl;
      std::tie(meshSource, geometrySource) = createCylinder(radius, height, numPoints);
      //(0.1 1 1 50) more detailed
      //(0.2 1 1.3 20) rough but works fast and converges
      //(0.2 1 1.5 20) degenerate
      break; }
    case 3: { // Minimal surface from cube with holes
      if (argc != 4) {
        std::cout << "Usage 3 <mesh_lenght> <offset>" << std::endl;
        return EXIT_FAILURE;
      }
      std::cout << "offset = " << atof(argv[3]) << std::endl;
      // Import and deform the cube
      std::tie(meshSource, geometrySource) = readManifoldSurfaceMesh("../../data/cube_hole.obj");
      cubeDeform(atof(argv[3]));
      //(O.25 0.3) slow but detailed
      //(0.4 0.4) rough but fast and degenerate
      break; }
    case 4: { // Minimal surface from imported mesh
      if (argc != 4) {
        std::cout << "Usage 4 <mesh_lenght> <path_to_mesh>" << std::endl;
        return EXIT_FAILURE;
      }
      // Define usual data path and concatenate
      std::string dataDir = "../../data/";
      dataDir += argv[3];
      std::cout << "path = " << dataDir << std::endl;
      std::tie(meshSource, geometrySource) = readManifoldSurfaceMesh(dataDir);
      break; }
  }

  // Remesh the mesh if needed
  meshLength = atof(argv[2]);
  if (meshLength > 0) {
    std::cout << "mesh_length = " << atof(argv[2]) << std::endl;
    RemeshOptions options;
    options.targetEdgeLength = atof(argv[2]);
    remesh(*meshSource, *geometrySource, options);
  } else {
    std::cout << "no remeshing" << std::endl;
  }
  
  // Create the curve to display on the boundary
  std::tie(curvePoints, curveEdges) = curveOnBoundary();
  auto psCurve = polyscope::registerCurveNetwork("Curve", curvePoints, curveEdges);

  auto psMesh = polyscope::registerSurfaceMesh("Minimal Surface",
                                 geometrySource->inputVertexPositions,
                                 meshSource->getFaceVertexList());

  // This section, including the scalar field, has grahical purpose, to approach the look of bubbles
  psMesh->setTransparency(0.5);
  psMesh->setMaterial("candy");
  psCurve->setMaterial("candy");

  auto f = [](const Vector3& p) {
  float r = sqrt(p.x*p.x + p.y*p.y + p.z*p.z);
  return sin(20*r + 10*sin(3*p.x)*sin(4*p.y)*sin(3*p.z));
  }; 
  auto scalarValues = functionToScalarField(f);

  psMesh->addVertexScalarQuantity("'Soap' texture", scalarValues);

  // Specify the callback
  polyscope::state::userCallback = myCallback;

  // Give control to the polyscope gui
  polyscope::show();

  return 0;
}
