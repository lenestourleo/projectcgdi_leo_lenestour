
###Compilation Steps :

mkdir build ; cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
cd MinimalSurfaces

###How to Use :
You can use the following arguments for ./minimal
- 1 <mesh_lenght> <num_points> : creates the minimal surface starting from the curve parametrization of the boundary
- 2 <mesh_lenght> <radius> <height> <num_points> : creates the minimal surface starting from the created cylinder mesh
- 3 <mesh_lenght> <offset> : creates the minimal surface starting from the imported mesh of the cube with holes and specify the offsets for variations
- 4 <mesh_lenght> <path_to_mesh> : creates the minimal surface starting from an imported mesh

Best examples : 
-1 0.2 50
-2 0.2 1 1.3 20 (rough but works fast and converges)
-2 0.2 1 1.5 20 (degenerates)
-3 O.25 0.3 (slow but detailed)
-3 0.4 0.4 (rough but fast and degenerate)

###Please let me know if there is a problem with the compilation steps, it is most likely due to a mistake in the settings of the project
